<div _ngcontent-c42="" class="container-fluid p-4"><div _ngcontent-c42="" class="row mt-4"><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/integration.md"><h1 id="tutorial-de-integração">Tutorial de integração</h1>
<p>Para integrar o <strong>Imobisales</strong> ao seu software favorito, basta seguir os passos abaixo. É
simples, rápido e seguro.</p>
<p>Cada solicitação para a API deve ter como URL base:
BASE_URL: <a href="http://app.imobisales.com.br/captei/rest">http://app.imobisales.com.br/captei/rest</a></p>
<p>Token de autenticação: Deve ser gerado no menu Configurações > Autenticação: Clicando no botão Gerar Token</p>

<p>Ou</p>

<pre class=" language-start"><code class=" language-start">POST /captei/rest/authenticate HTTP/1.1
Host: https://app.imobisales.com.br/captei/rest
Content-Type: application/json
Cache-Control: no-cache
{
  username: "usuario@imobisales.com.br", 
  password: "12345678"
}
</code></pre>
<p>O response da autenticação será: </p>
<pre class=" language-start"><code class=" language-start">{
  "authorizationToken":"Bearer eyJhbGciOiJIUzDtIiJ9.eyXzdWIiOiJhbmRlcnNvbkByaWJlaXJvZWFycmFiYWwuY29tLmJyIiwiY29tcGFueSI6eyJpZCI6IjVjZGI1NzBlM2QxZmVjMDAwMTUxZmY5NCJ9LCJyb2xlcyI6IkFETUlOX1JPTEUiLCJleHAiOjE1OTU5NDY0MjB9.fKUQPdGrAaB6Nz1DeW9oiQv8PiPl_G0pGpw7OihPg8ua8u4iCJ0hqpukZeJA37-hzs7mGS5soVRw69l1Mtj62M",
  "refreshToken":"eyJhbGciOiJIUzUxMiJ3.eyJzdWIiOiJhbmRlcnNvbkByaWJlaXJvZWFycmFiYWwuY29tLmJyIiwiZXhwIjoxNTk1OTUwMDIwfQ.gNGPe0xGgWSS7NcJY22X-qWq3R6cufjeinaJm_oVdBBcj1bJ6hlOnbP2bBnQnod_Fq9bVOchsPBc1Amr6IltXJ"}
</code></pre>



<p>Se você tem alguma dúvida ou encontrou problemas, não hesite em nos contatar através de:</p>
<ul>
<li>Whatsapp: Entre em contato pelo número +55(32)99831-5387</li>
<li>E - mail: Entre em contato pelo e-mail ( <a href="mailto:guilherme.caputo@grupoemidia.com">guilherme.caputo@grupoemidia.com</a> )</li>
</ul>
</markdown><br _ngcontent-c42=""><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/lead/leadPostIntegration.md"><h1 id="lead-post">Lead POST</h1>
<p>Toda requisição POST, em Leads, deverá obrigatóriamente seguir o exemplo abaixo, para uma perfeita integração entre os dois software. Os campos a seguir são obrigatórios:</p>
<ul>
<li><strong>title</strong></li>
<li><strong>owner</strong></li>
<li><strong>stage</strong></li>
<li><strong>value</strong></li>
<li><strong>source</strong></li>
<li><strong>openingDate</strong></li>
<li><strong>previsionDateClosing</strong></li>
<li><strong>contactName</strong></li>
<li><strong>email</strong></li>
</ul>
<pre class=" language-start"><code class=" language-start">POST /captei/rest/lead/register HTTP/1.1
Host: https://app.imobisales.com.br/captei/rest
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO
{
   "title":"Nome do neǵocio",
   "value":1000.50,
   "source":"Formulário de Contato",
   "owner":{
      "username":"grupoemedia@gmail.com"
   },
   "stage":"Prospectado",
   "contact":{
      "name":"Nome do Lead",
      "email":"emaildolead@gmail.com",
      "telephone":"1199999999",
      "cellphone":"11999999999"
   },
   "previsionDateClosing":"2019-03-21T18:40:35.908Z",
   "openingDate":"2019-03-20T18:39:04.949Z",
   "Campo Dinamico Um": "Valor do campo dinamico um",
   "Campo Dinamico Dois": "Valor do campo dinamico dois",
   "Campo Dinamico Três": "Valor do campo dinamico três"
}
</code></pre>
</markdown><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/lead/leadGetIntegration.md"><h1 id="lead-get">Lead GET</h1>
<p>Toda requisição <strong>GET</strong>, em Leads, deverá obrigatóriamente seguir os exemplos abaixo, para uma perfeita integração entre os dois software. Existem três tipos possiveis de GET:</p>
<ul>
<li><strong>Por Id</strong></li>
<li><strong>Buscando todas as estapas com resultado paginado</strong></li>
<li><strong>Pelo Id da etapa e com resultado paginado</strong></li>
</ul>
<h4 id="exemplo-por-id">Exemplo por id:</h4>
<pre class=" language-start"><code class=" language-start">GET /captei/rest/lead/by-id/5ba54afd0c57860001d4905b
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO</code></pre>
<h4 id="exemplo-buscando-todas-as-estapas-com-resultado-paginado">Exemplo buscando todas as estapas com resultado paginado:</h4>
<pre class=" language-start"><code class=" language-start">GET /captei/rest/lead/in-all-stages?page=0&amp;size=5
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO</code></pre>
<h4 id="exemplo-pelo-id-da-etapa-e-com-resultado-paginado">Exemplo pelo Id da etapa e com resultado paginado:</h4>
<pre class=" language-start"><code class=" language-start">GET /capitei/rest/lead/?page=1&amp;size=5&amp;stageId=5b71d989ffd6de535fd456eb
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO</code></pre>
</markdown><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/lead/leadPutIntegration.md"><h1 id="lead-put">Lead PUT</h1>
<p>Toda requisição <strong>PUT</strong>, em Leads, deverá obrigatóriamente seguir os exemplos abaixo, para uma perfeita integração entre os dois software. Existem três tipos possiveis de PUT:</p>
<ul>
<li><strong>Por Leads Pedidos</strong></li>
<li><strong>Atualização simples</strong></li>
<li><strong>Atualização de Status</strong></li>
</ul>
<h4 id="exemplo-por-leads-pedidos">Exemplo por leads pedidos</h4>
<pre class=" language-start"><code class=" language-start">PUT /captei/rest/lead/lost-lead/5c508380a8bc0605041f540d
Host: localhost:8080
Content-Type: application/json
Authorization: TOKEN_GERADO

{
"reason": "asdasdasdasd"
}</code></pre>
<h4 id="exemplo-de-atualização-simples">Exemplo de atualização simples</h4>
<pre class=" language-start"><code class=" language-start">PUT /capitei/rest/lead/update HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO
{
"id":"5b7582f596315d446b465911",
"name":"Cliente Name edit",
"contact": {
"id": "5b7582f496315d446b465910",
"name": "contato ae",
"telephone": "(32)888888888888",
"email": "teste123@gmail.com"
},
"stage":"Sem contato",
"Sexo":"Outros",
"CPF":"1111111111111111",
"Instituicao":"UFJF",
"Curso":"OUTRO CURSO",
"Graduacao":"Bacharelado",
"Novo atributo": "Valor do novo atributo"
}</code></pre>
<h4 id="exemplo-de-atualização-de-status">Exemplo de atualização de Status</h4>
<pre class=" language-start"><code class=" language-start">PUT /capitei/rest/lead/update-status HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO

{"id":"5b7582f596315d446b465911", "stage":"Sem contato"}</code></pre>
</markdown><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/task/taskGetIntegration.md"><h1 id="task-get">Task GET</h1>
<p>Toda requisição <strong>GET</strong>, em Tasks, deverá obrigatóriamente seguir os exemplos abaixo, para uma perfeita integração entre os dois software. Existem dois tipos possiveis de GET:</p>
<ul>
<li><strong>Por Calendar</strong></li>
<li><strong>Busca paginada</strong></li>
</ul>
<h4 id="exemplo-por-id">Exemplo por id:</h4>
<pre class=" language-start"><code class=" language-start">GET /captei/rest/task/calendar
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO</code></pre>
<h4 id="exemplo-busca-paginada">Exemplo busca paginada:</h4>
<pre class=" language-start"><code class=" language-start">GET /captei/rest/task/?page=0&amp;size=1
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO</code></pre>
</markdown><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/task/taskPostIntegration.md"><h1 id="task-post">Task POST</h1>
<p>Toda requisição POST, em Task, deverá obrigatóriamente seguir o exemplo abaixo, para uma perfeita integração entre os dois software. Todos os campos são obrigatórios.</p>
<pre class=" language-start"><code class=" language-start">POST /captei/rest/task/5ba549ef0c57860001d49057/
Host: localhost:8080
Content-Type: application/json
Cache-Control: no-cache
Authorization: TOKEN_GERADO


{
    "activities": [
    ],
    "assign": {
        "id": "5ba4ebdeabc10e0001e07f79",
        "name": "Diogo Garcia"
    },
    "description": "mais um teste aqui manual infelizmente",
    "name": "mais teste",
    "startDate": "2018-10-11T20:11:00.580Z",
    "type": "EMAIL"
}</code></pre>
</markdown><markdown _ngcontent-c42="" class="variable-binding" src="/assets/markdown/task/taskPutIntegration.md"><h1 id="task-put">Task PUT</h1>
<p>Toda requisição <strong>PUT</strong>, em Leads, deverá obrigatóriamente seguir o exemplo abaixo, para uma perfeita integração entre os dois software. O update é feito da seguinte forma:</p>
<h4 id="exemplo-por-leads-pedidos">Exemplo por leads pedidos</h4>
<pre class=" language-start"><code class=" language-start">PUT /captei/rest/task
Host: localhost:8080
Cache-Control: no-cache
Content-Type: application/json
Authorization: TOKEN_GERADO

{
   "id":"5bb5038cd01b06053bfff5f0",
   "name":"Editado",
   "description":"descrição do teste editado",
   "status":"NAO_INICIADO",
   "type":"REUNION",
   "creationDate":[
      2018,
      10,
      3,
      17,
      59,
      40,
      7000000
   ],
   "startDate":"2018-10-23T05:59:00.000Z",
   "dueDate":null,
   "assign":{
      "id":"5ba4ebdeabc10e0001e07f79",
      "name":"Diogo Garcia",
      "username":"grupoemedia@gmail.com",
      "enabled":true,
      "expired":false,
      "company":{
         "id":"5ba4ebddabc10e0001e07f71",
         "name":"grupo emedia",
         "segment":"Serviços de TI",
         "numberEmployees":"10",
         "telephone":"3232122302",
         "cnpj":"89.384.727/0001-23"
      },
      "roles":[
         {
            "id":"5ba4e49f7f65c466b7c4ef53",
            "name":"ADMIN_ROLE",
            "authority":"ADMIN_ROLE"
         }
      ]
   },
   "company":{
      "id":"5ba4ebddabc10e0001e07f71",
      "name":"grupo emedia",
      "segment":"Serviços de TI",
      "numberEmployees":"10",
      "telephone":"3232122302",
      "cnpj":"89.384.727/0001-23",
      "address":null,
      "creditSms":null
   },
   "notes":[],
   "activities":[{"activity":"lorem ipsum"}]
}</code></pre>
</markdown></div></div>